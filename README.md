# Masters project

Procedural generation of cave systems in Unity

## Description
A project for master's thesis in computer graphics at Universty of Electrical Engineering and Computing.
The player can explore a dynamically generated endless environment with caves generated using Perlin worms.
Demonstration video: https://youtu.be/WlLKdd6jOBk