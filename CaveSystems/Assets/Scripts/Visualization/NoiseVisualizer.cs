using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseVisualizer : MonoBehaviour
{
    public int size = 128;
    [Range(0,1)]
    public float threshold = 0.5f;
    public float scale = 1.1f;
    public Renderer textureRenderer;
    public bool autoUpdate = true;
    public bool quantize = true;
    float[,] vignette;
    public int vignetteSide = 512;
    public int offsetX = 0;
    [Range(1, 10)]
    public int whiteNoiseIterations = 1;
    [Range(1, 20)]
    public float whiteNoiseMagic = 1;

    private void Start()
    {
        InitializeVignette(vignetteSide);
        GenerateNoise();
    }

    public void InitializeVignette(int sideLength)
    {
        vignette = new float[sideLength, sideLength];
        float radius = sideLength * 0.5f;
        Vector2 center = new Vector2(radius, radius);
        
        for(int i = 0; i < sideLength; i++)
        {
            for( int j = 0; j < sideLength; j++)
            {
                float distanceRatio = Vector2.Distance(new Vector2(i, j), center) / radius;
                vignette[i,j] = Mathf.Lerp(0, 1, Mathf.Clamp01(distanceRatio));
            }
        }
    }

    public void DrawNoiseMap(float[,] noiseMap)
    {
        int width = noiseMap.GetLength(0);
        int height = noiseMap.GetLength(1);

        Texture2D texture = new Texture2D(width, height);

        Color[] colorMap = new Color[width * height];
        for(int x = 0; x < width; x++)
        {
            for(int y = 0; y < height; y++)
            {
                float value = noiseMap[x, y];
                if (quantize)
                {
                    value = value > threshold ? 1 : 0;

                }
                colorMap[y * width + x] = Color.Lerp(Color.black, Color.white, value);
            }
        }
        texture.SetPixels(colorMap);
        texture.Apply();
        textureRenderer.sharedMaterial.mainTexture = texture;
        textureRenderer.transform.localScale = new Vector3(width, 1, height);
    }

    public float[,] GeneratePerlinNoise()
    {
        float[,] noiseMap = new float[size, size];
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                float value = Mathf.PerlinNoise((i + offsetX) / scale, j / scale);
                noiseMap[i, j] = value;
            }
        }
        return noiseMap;
    }

    public float[,] GenerateFastNoise()
    {
        FastNoiseLite noise = new FastNoiseLite();
        noise.SetNoiseType(FastNoiseLite.NoiseType.Cellular);
        noise.SetFractalType(FastNoiseLite.FractalType.Ridged);
        noise.SetFractalOctaves(1);

        // Gather noise data
        float[,] noiseMap = new float[size, size];


        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                noiseMap[x, y] = noise.GetNoise((x + offsetX) / scale, y / scale);
            }
        }
        return noiseMap;
    }

    public float[,] GenerateWhiteNoise()
    {
        float[,] noiseMap = new float[size, size];

        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                noiseMap[x, y] = Noise.WhiteNoise((x + offsetX) / scale, y / scale);
                for (int k = 1; k < whiteNoiseIterations; k++)
                {
                    noiseMap[x, y] -= Noise.WhiteNoise((x + offsetX + k * whiteNoiseMagic) / scale, y / scale);
                }
                noiseMap[x, y] = Mathf.Clamp01(noiseMap[x, y]);
            }
        }
        return noiseMap;
    }

    public void GenerateNoise(bool usesVignette = false)
    {
        float[,] noiseMap = GenerateFastNoise();
        if (usesVignette)
        {
            ApplyVignette(noiseMap);
        }
        DrawNoiseMap(noiseMap);
    }

    public void ApplyVignette(float[,] noiseMap)
    {
        int width = noiseMap.GetLength(0);
        int height = noiseMap.GetLength(1);
        int step = vignetteSide / width;
        
        Vector2 center = new Vector2(width * 0.5f, height * 0.5f);
        for(int i = 0; i < width; i++)
        {
            for(int j = 0; j < height; j++)
            {
                float vignetteValue = vignette[i * step, j * step];
                if(vignetteValue == 1)
                {
                    noiseMap[i, j] = 1;
                }
                else
                {
                    noiseMap[i, j] *= vignetteValue;
                }
               
            }
        }
    }
}
