using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;

public class ChunkGenerator
{
    int width, height, length, maxHeight, bottomPadding;
    float wormSegmentRadiusNoiseScale;
    int surfaceChunkNumber, undergroundChunkNumber;
    int octaves;
    float scale, thresholdDensity = 0.5f;
    bool smoothTerrain = true, flatShaded = false;
    public Queue<Utilities.ColumnDensityCallback> densityCallbacks;
    public Queue<Utilities.ColumnMeshDataCallback> meshDataCallbacks;

    public ChunkGenerator(int width, int height, int length, int maxHeight, int bottomPadding,
        float wormSegmentRadiusNoiseScale, int surfaceChunkNumber, int undergroundChunkNumber,
        int octaves, float scale)
    {
        this.width = width;
        this.height = height;
        this.length = length;
        this.maxHeight = maxHeight;
        this.bottomPadding = bottomPadding;
        this.wormSegmentRadiusNoiseScale = wormSegmentRadiusNoiseScale;
        this.surfaceChunkNumber = surfaceChunkNumber;
        this.undergroundChunkNumber = undergroundChunkNumber;
        this.octaves = octaves;
        this.scale = scale;

        densityCallbacks = new Queue<Utilities.ColumnDensityCallback>();
        meshDataCallbacks = new Queue<Utilities.ColumnMeshDataCallback>();
    }
    public void RequestColumnDensities(Action<List<float[,,]>, Vector3Int> callback, Vector3Int originChunkCoords, 
        List<PerlinWorm> loadedWorms)
    {
        ThreadStart threadStart = delegate
        {
            ColumnDensityThread(callback, originChunkCoords, loadedWorms);
        };
        new Thread(threadStart).Start();
    }

    void ColumnDensityThread(Action<List<float[,,]>, Vector3Int> callback, Vector3Int originChunkCoords,
            List<PerlinWorm> loadedWorms)
    {
        Vector3Int seaLevelChunkLocation = Utilities.getChunkLocationFromChunkCoords(originChunkCoords,
                        width, height, length);
        float[,] heightMap = Noise.GenerateHeightMap(width + 1, length + 1, octaves, scale, seaLevelChunkLocation);
        List<float[,,]> column = new List<float[,,]>();
        bool needsNewVerticalChunk = true;
        for (int h = 0; h < surfaceChunkNumber + undergroundChunkNumber; h++)
        {
            float[,,] terrainMap;
            int chunkY = h - undergroundChunkNumber;
            Vector3Int chunkCoords = new Vector3Int(originChunkCoords.x,
                        chunkY, originChunkCoords.z);
            
            if (chunkY < 0)
            {
                terrainMap = GenerateChunk(chunkCoords, loadedWorms, null);
            }
            else
            {
                terrainMap = GenerateChunk(chunkCoords, loadedWorms, heightMap, needsNewVerticalChunk);
                needsNewVerticalChunk = Utilities.NeedsNewVerticalChunk(terrainMap, width, height, length);
            }

            column.Add(terrainMap);
        }
        lock (densityCallbacks)
        {
            densityCallbacks.Enqueue(new Utilities.ColumnDensityCallback(originChunkCoords, column, callback));
        }
    }

    public float[,,] GenerateChunk(Vector3Int chunkCoords, List<PerlinWorm> loadedWorms, 
        float[,] heightMap = null, bool needsNewVerticalChunk = true)
    {
        float[,,] terrainMap;
        List<PerlinWorm> activeWorms = Utilities.GetAllWormsPassingThroughChunk(chunkCoords, loadedWorms);
        if (chunkCoords.y < 0)
        {
            if (activeWorms.Count == 0)
            {
                // chunk is empty and doesn't need to be generated or drawn
                return null;
            }
            else
            {
                // cave goes through chunk, values initialized to 0 which is GROUND_DENSITY
                terrainMap = new float[width + 1, height + 1, length + 1];
            }
        }
        else if (chunkCoords.y == 0 || needsNewVerticalChunk)
        {
            Vector3Int chunkLocation = Utilities.getChunkLocationFromChunkCoords(chunkCoords, width, height, length);
            terrainMap = Noise.ConvertHeightMapForMarchingCubes(width, height, length, chunkLocation,
                maxHeight, bottomPadding, heightMap);
            
        }
        else
        {
            // top chunk is empty and doesn't need to be generated or drawn
            return null;
        }
        BuildCaves(chunkCoords, activeWorms, terrainMap);
        return terrainMap;
    }

    public void BuildCaves(Vector3Int chunkCoords, List<PerlinWorm> activeWorms, float[,,] terrainMap)
    {
        int wormNum = 0;
        foreach (PerlinWorm worm in activeWorms)
        {
            wormNum++;
            for (int l = 0; l < worm.segmentPositions.Count; l++)
            {
                Vector3 segment = worm.segmentPositions[l];
                int segmentRadius = worm.segmentRadiuses[l];

                if (worm.SegmentAffectsChunk(l, chunkCoords))
                {
                    Vector3Int localCoords = Utilities.getLocalCoordsInsideChunk(chunkCoords, segment, width, height, length);
                    int startX = Mathf.Max(0, localCoords.x - segmentRadius);
                    int endX = Mathf.Min(width + 1, localCoords.x + segmentRadius);
                    int startY = Mathf.Max(0, localCoords.y - segmentRadius);
                    int endY = Mathf.Min(height + 1, localCoords.y + segmentRadius);
                    int startZ = Mathf.Max(0, localCoords.z - segmentRadius);
                    int endZ = Mathf.Min(length + 1, localCoords.z + segmentRadius);
                    for (int i = startX; i < endX; i++)
                    {
                        for (int j = startY; j < endY; j++)
                        {
                            for (int k = startZ; k < endZ; k++)
                            {
                                terrainMap[i, j, k] =
                                    Utilities.GetBuildCavePointValueVariableRadius(localCoords, i, j, k,
                                    segmentRadius, segment, terrainMap[i, j, k], wormSegmentRadiusNoiseScale);
                            }
                        }
                    }
                }
            }
        }
    }

    public void RequestColumnMeshData(Action<List<Utilities.MeshData>, Vector3Int> callback, Vector3Int originChunkCoords,
    List<float[,,]> densities)
    {
        ThreadStart threadStart = delegate
        {
            ColumnMeshDataThread(callback, originChunkCoords, densities);
        };
        new Thread(threadStart).Start();
    }
    void ColumnMeshDataThread(Action<List<Utilities.MeshData>, Vector3Int> callback, Vector3Int originChunkCoords,
        List<float[,,]> densities)
    {
        List<Utilities.MeshData> columnMeshData = new List<Utilities.MeshData>();
        for (int i = 0; i < densities.Count; i++)
        {
            float[,,] densityMap = densities[i];
            if (densityMap != null)
            {
                columnMeshData.Add(MarchingCubes.MarchCubes(width, height, length, densityMap, 
                    thresholdDensity, smoothTerrain, flatShaded));
            }
            else
            {
                columnMeshData.Add(new Utilities.MeshData(new List<Vector3>(), new List<int>()));
            }
        }
        lock (meshDataCallbacks)
        {
            meshDataCallbacks.Enqueue(new Utilities.ColumnMeshDataCallback(originChunkCoords, columnMeshData, callback));
        }
    }
}

