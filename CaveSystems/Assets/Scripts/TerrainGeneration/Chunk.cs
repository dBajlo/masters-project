using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk : MonoBehaviour
{
    //actual coordinates
    public Vector3Int location;
    //coordinates in chunk space
    public Vector3Int chunkCoords;
    public Mesh mesh;

    MeshFilter meshFilter;
    MeshRenderer meshRenderer;
    MeshCollider meshCollider;

    // Add components/get references in case lost (references can be lost when working in the editor)
    public void Initiate(Vector3Int _location, Vector3Int _chunkCoords, Material material)
    {
        meshFilter = gameObject.AddComponent<MeshFilter>();
        meshRenderer = gameObject.AddComponent<MeshRenderer>();
        meshCollider = gameObject.AddComponent<MeshCollider>();

        mesh = new Mesh();
        meshFilter.sharedMesh = mesh;
        meshCollider.sharedMesh = mesh;
        meshRenderer.material = material;

        chunkCoords = _chunkCoords;
        location = _location;
        transform.position = location;

        meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;
    }

    public void BuildMesh(List<Vector3> vertices, List<int> triangles)
    {
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();

        meshFilter.mesh = mesh;
        meshCollider.sharedMesh = mesh;
    }

    public static bool ChunkContainsWormHead(Vector3Int chunkLocation)
    {
        int octaves = 1;
        float scale = 0.9f;
        float x = chunkLocation.x + chunkLocation.y + Noise.seed;
        float y = chunkLocation.z + chunkLocation.y;

        float value = Noise.GetPerlinNoise(x, y, scale, octaves);
        return value > Constants.CHUNK_CONTAIN_WORM_HEAD_THRESHOLD;
    }
}
