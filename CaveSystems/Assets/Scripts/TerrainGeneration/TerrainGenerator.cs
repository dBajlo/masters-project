﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
	public Material surfaceChunkMaterial;
	public Material undergroundChunkMaterial;

	[Range(2, 6)]
	public int perlinNum = 2;
	[Range(1, 10)]
	public int octaves = 3;
	public const int maxOctaves = 10;
	public float scale = 16.43f;

	public bool smoothTerrain = true;
	public bool flatShaded = true;
	public bool autoUpdate = true;

	public float offsetX = 64;
	public float offsetY = 64;
	public float offsetZ = 64;
	[Range(0, 1)]
	public float thresholdDensity = 0.5f;

	[Range(2, 100)]
	public int width = 16;
	[Range(2, 100)]
	public int height = 64;
	[Range(2, 100)]
	public int length = 16;
	int maxHeight;
	public int undergroundChunkNumber;
	public int surfaceChunkNumber;
	public int bottomPadding = 3;

	public GameObject player;
	const int chunkAddDistance = 8;
	const float sqrChunkRemoveDistance = (chunkAddDistance + 3) * (chunkAddDistance + 3);
	int caveAddDistance;
	float sqrCaveRemoveDistance;
	List<List<GameObject>> loadedChunkColumns = new List<List<GameObject>>();

	ChunkGenerator chunkGenerator;
	bool canReload = true;

	//worms
	List<PerlinWorm> loadedWorms;
	WormGenerator wormGenerator;
	public GameObject wormSegmentPrefab;
	public GameObject caveMarkerPrefab;
	public bool drawWorm = false;
	public bool caveMarkerEnabled = true;

	public float wormDirectionNoiseScale = 37.1f;
	public int wormDirectionNoiseOctaves = 1;
	public float wormSegmentRadiusNoiseScale = 11.1f;
	public int wormSegmentRadiusNoiseOctaves = 1;

	public int segmentNumber = 50;
	public float segmentLength = 2f;

	private void Start()
	{
		maxHeight = surfaceChunkNumber * height;
        UnityEngine.Random.InitState(Noise.seed);
		player = GameObject.Find("Player");
		Vector3 startingChunkPosition = Vector3.zero;
        if (player)
        {
			startingChunkPosition = player.transform.position;
		}

		//assumes width == height == length
		caveAddDistance = chunkAddDistance + Mathf.CeilToInt(Constants.maxWormLength / width);
		sqrCaveRemoveDistance = Mathf.Pow(caveAddDistance + 2, 2);

		wormGenerator = new WormGenerator(wormSegmentPrefab, caveMarkerPrefab, width, height, length, 
			wormDirectionNoiseScale, wormDirectionNoiseOctaves, 
			wormSegmentRadiusNoiseScale, wormSegmentRadiusNoiseOctaves, segmentLength);
		loadedWorms = new List<PerlinWorm>();
		chunkGenerator = new ChunkGenerator(width, height, length, maxHeight, bottomPadding,
			wormSegmentRadiusNoiseScale, surfaceChunkNumber, undergroundChunkNumber, octaves, scale);

		GenerateTerrain(Utilities.getChunkCoordsFromPosition(startingChunkPosition, width, height, length));
	}

    private void Update()
    {
		if(chunkGenerator.densityCallbacks.Count == 0 && chunkGenerator.meshDataCallbacks.Count == 0)
        {
			canReload = true;
        }
        else
        {
			canReload = false;
        }
        if(chunkGenerator.densityCallbacks.Count > 0)
        {
			Utilities.ColumnDensityCallback columnDensityCallback = chunkGenerator.densityCallbacks.Dequeue();
			columnDensityCallback.callback(columnDensityCallback.densities, columnDensityCallback.originChunkCoords);
        }
		if(chunkGenerator.meshDataCallbacks.Count > 0)
        {
			Utilities.ColumnMeshDataCallback columnMeshDataCallback = chunkGenerator.meshDataCallbacks.Dequeue();
			columnMeshDataCallback.callback(columnMeshDataCallback.columnMeshData, columnMeshDataCallback.originChunkCoords);
		}
    }

    public void ReloadChunks(Vector3Int centerChunkCoords)
    {
        if (canReload)
        {
			List<Vector3Int> existingOriginChunksCoords = DestroyChunksOutOfBounds(centerChunkCoords);
			HashSet<Vector3> existingWorms = DestroyWormsOutOfBounds(centerChunkCoords);
			GenerateTerrain(centerChunkCoords, existingOriginChunksCoords, existingWorms);
		}
    }

	public List<Vector3Int> DestroyChunksOutOfBounds(Vector3Int centerChunkCoords)
    {
		List<Vector3Int> existingOriginChunksCoords = new List<Vector3Int>();
		List<List<GameObject>> columnsToDestroy = new List<List<GameObject>>();
		foreach(List<GameObject> chunkColumn in loadedChunkColumns)
        {
			GameObject originChunk = chunkColumn[undergroundChunkNumber];
			Vector3Int chunkCoords = originChunk.GetComponent<Chunk>().chunkCoords;
			if (Utilities.chunkOutOfRenderDistance(chunkCoords, centerChunkCoords, sqrChunkRemoveDistance))
            {
				columnsToDestroy.Add(chunkColumn);
			}
            else
            {
				existingOriginChunksCoords.Add(chunkCoords);
            }
        }
		DestroyChunks(columnsToDestroy);
		return existingOriginChunksCoords;
	}

	public HashSet<Vector3> DestroyWormsOutOfBounds(Vector3Int centerChunkCoords)
	{
		HashSet<Vector3> existingWorms = new HashSet<Vector3>();
		List<PerlinWorm> wormsToDestroy = new List<PerlinWorm>();
		foreach (PerlinWorm worm in loadedWorms)
		{
			Vector3 headPosition = worm.headPosition;
			Vector3Int headChunk = Utilities.getChunkCoordsFromPosition(headPosition, width, height, length);
			if (Utilities.chunkOutOfRenderDistance(headChunk, centerChunkCoords, sqrCaveRemoveDistance))
			{
				wormsToDestroy.Add(worm);
			}
			else
			{
				existingWorms.Add(headChunk);
			}
		}
		for (int i = 0; i < wormsToDestroy.Count; i++)
		{
			PerlinWorm worm = wormsToDestroy[0];
			worm.DeleteDrawnSegments();
			wormsToDestroy.RemoveAt(0);
			loadedWorms.Remove(worm);
		}
		return existingWorms;
	}

	public void DestroyChunks(List<List<GameObject>> columns)
    {
		for (int i = 0; i < columns.Count; i++)
		{
			List<GameObject> column = columns[i];
			int columnHeight = column.Count;
			for (int j = 0; j < columnHeight; j++)
            {
				GameObject chunk = column[0];
				column.RemoveAt(0);
				if(chunk != null)
                {
					Destroy(chunk);
				}
			}
			loadedChunkColumns.Remove(column);
		}
	}

	//chunk location is the actual coordinates of chunk origin
	//chunk coords is the coordinates in chunk space
	public void GenerateTerrain(Vector3Int centerChunkCoords, List<Vector3Int> existingOriginChunksCoords = null, 
		HashSet<Vector3> existingWorms = null)
    {
		for (int i = 0; i < 2 * caveAddDistance; i++)
		{
			for (int j = 0; j < 2 * caveAddDistance; j++)
			{
				Vector3Int chunkCoords = new Vector3Int(-caveAddDistance + i + centerChunkCoords.x,
					0, -caveAddDistance + j + centerChunkCoords.z);
				Vector3Int chunkLocation = Utilities.getChunkLocationFromChunkCoords(chunkCoords, width, height, length);

				// if(existingWorms != null) Debug.Log("existing worms num " + existingWorms.Count);
				if (Chunk.ChunkContainsWormHead(chunkLocation) && 
					(existingWorms == null || !existingWorms.Contains(chunkCoords)))
				{
					PerlinWorm worm = wormGenerator.GenerateWorm(chunkLocation, caveMarkerEnabled, segmentNumber);
					if (drawWorm) worm.DrawWorm();
					loadedWorms.Add(worm);
				}
			}
		}

		for (int i = 0; i < 2 * chunkAddDistance; i++)
        {
			for(int j = 0; j < 2 * chunkAddDistance; j++)
            {
				Vector3Int originChunkCoords = new Vector3Int(-chunkAddDistance + i + centerChunkCoords.x,
					0, -chunkAddDistance + j + centerChunkCoords.z);
				if (existingOriginChunksCoords == null || !existingOriginChunksCoords.Contains(originChunkCoords))
                {
					chunkGenerator.RequestColumnDensities(OnColumnDensitiesGenerated, originChunkCoords, loadedWorms);
				}
			}
        }
    }

	public void OnColumnDensitiesGenerated(List<float[,,]> densities, Vector3Int originChunkCoords)
    {
		chunkGenerator.RequestColumnMeshData(OnColumnMeshDataReceived, originChunkCoords, densities);
    }

	public void OnColumnMeshDataReceived(List<Utilities.MeshData> meshDatas, Vector3Int originChunkCoords)
    {
		List<GameObject> column = new List<GameObject>();
		for(int i = 0; i < meshDatas.Count; i++)
        {
			Utilities.MeshData meshData = meshDatas[i];
			int chunkY = i - undergroundChunkNumber;
			Vector3Int chunkCoords = new Vector3Int(originChunkCoords.x, chunkY, originChunkCoords.z);
			GameObject chunk = BuildMesh(chunkCoords, meshData.vertices, meshData.triangles);
			column.Add(chunk);
		}
		loadedChunkColumns.Add(column);
	}


	GameObject BuildMesh(Vector3Int chunkCoords, List<Vector3> vertices, List<int> triangles)
	{
		Vector3Int chunkLocation = Utilities.getChunkLocationFromChunkCoords(chunkCoords, width, height, length);
		GameObject chunk = new GameObject();
		chunk.transform.parent = transform;
		chunk.AddComponent<Chunk>();
		Chunk chunkScript = chunk.GetComponent<Chunk>();
		Material chunkMaterial = chunkCoords.y >= 0 ? surfaceChunkMaterial : undergroundChunkMaterial;
		chunkScript.Initiate(chunkLocation, chunkCoords, chunkMaterial);
		chunkScript.BuildMesh(vertices, triangles);
		return chunk;
	}
}
