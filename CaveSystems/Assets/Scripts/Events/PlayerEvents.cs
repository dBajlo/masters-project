using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEvents : MonoBehaviour
{
    public TerrainGenerator terrainGenerator;
    public GameObject player;
    public float updateRate = 0.5f;

    public bool notifyPlayerNewChunk = true;

    private Vector3Int lastPlayerChunkLocation;
    private int width, height, length;

    private float nextUpdateTimestamp = 0;

    // Start is called before the first frame update
    void Start()
    {
        terrainGenerator = GameObject.Find("TerrainGenerator").GetComponent<TerrainGenerator>();
        width = terrainGenerator.width;
        height = terrainGenerator.height;
        length = terrainGenerator.length;
        player = GameObject.Find("Player");
        lastPlayerChunkLocation = Utilities.getChunkCoordsFromPosition(
            player.transform.position, width, height, length);
    }

    // Update is called once per frame
    void Update()
    {
        float currentTime = Time.realtimeSinceStartup;
        if (currentTime >= nextUpdateTimestamp)
        {
            nextUpdateTimestamp = currentTime + updateRate;
            if (notifyPlayerNewChunk)
            {
                Vector3Int currentChunkLocation = Utilities.getChunkCoordsFromPosition(
                player.transform.position, width, height, length);
                if (currentChunkLocation != lastPlayerChunkLocation)
                {
                    lastPlayerChunkLocation = currentChunkLocation;
                    terrainGenerator.ReloadChunks(lastPlayerChunkLocation);
                }
            }
        }
    }
}
