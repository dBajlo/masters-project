﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 7;
    float baseSpeed;
    public float mouseSensitivity = 100;
    public float jumpForce = 1f;
    public GameObject spotlight;
    float mouseX;
    float mouseY;
    float horizontal;
    float vertical;
    Rigidbody rigidBody;
    Quaternion deltaRotation;
    Vector3 deltaPosition;
    Transform cameraTransform;
    float xRotation = 0;
    Collider collider;
 
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rigidBody = GetComponent<Rigidbody>();
        cameraTransform = Camera.main.transform;
        spotlight = GameObject.Find("Spot Light");
        if (spotlight == null) Debug.Log("couldnt find spotligh");
        collider = GetComponent<Collider>();
        collider.enabled = rigidBody.useGravity;
        baseSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        GetInputs();
        float angleY = mouseY * mouseSensitivity * Time.deltaTime;
        xRotation -= angleY;
        xRotation = Mathf.Clamp(xRotation, -90, 90);
        cameraTransform.localRotation = Quaternion.Euler(xRotation, 0, 0);
        if(spotlight != null)
        {
            spotlight.transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
        }
        
        //Debug.Log(cameraTransform.rotation.eulerAngles);
        if (Input.GetButtonDown("Jump"))
        {
            rigidBody.velocity = Vector3.up * jumpForce;
            rigidBody.useGravity = true;
            collider.enabled = true;
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            rigidBody.useGravity = !rigidBody.useGravity;
            rigidBody.velocity = Vector3.zero;
            collider.enabled = !collider.enabled;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (speed > baseSpeed) speed = baseSpeed;
            else speed = baseSpeed * 3;
        }
    }

    private void FixedUpdate()
    {
        float angleX = mouseX * mouseSensitivity * Time.fixedDeltaTime;
        deltaRotation = Quaternion.Euler(Vector3.up * angleX);
        rigidBody.MoveRotation(rigidBody.rotation * deltaRotation);

        deltaPosition = ((transform.forward * vertical) + (transform.right * horizontal)) * Time.fixedDeltaTime * speed;
        rigidBody.MovePosition(rigidBody.position + deltaPosition);
    }

    void GetInputs()
    {
        mouseX = Input.GetAxis("Mouse X");
        mouseY = Input.GetAxis("Mouse Y");
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
    }
}
