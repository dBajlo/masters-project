using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarchingCubes
{
	public static int GetCubeConfig(float[] cube, float thresholdDensity)
	{
		int configIndex = 0;
		for (int i = 0; i < 8; i++)
		{
			if (cube[i] > thresholdDensity)
			{
				configIndex |= 1 << i;
			}
		}
		return configIndex;
	}

	public static Utilities.MeshData MarchCubes(int width, int height, int length, float[,,] terrainMap, 
		float thresholdDensity, bool smoothTerrain, bool flatShaded)
    {
		List<Vector3> vertices = new List<Vector3>();
		List<int> triangles = new List<int>();
		Dictionary<Vector3, int> vertexIndexDict = new Dictionary<Vector3, int>();
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				for (int z = 0; z < length; z++)
				{
					float[] cube = new float[8];
					for (int i = 0; i < 8; i++)
					{
						Vector3Int corner = new Vector3Int(x, y, z) + LookupTable.CornerTable[i];
						cube[i] = terrainMap[corner.x, corner.y, corner.z];
					}
					MarchingCubes.MarchCube(new Vector3Int(x, y, z), cube, vertices, triangles,
						thresholdDensity, smoothTerrain, flatShaded, vertexIndexDict);
				}
			}
		}
		return new Utilities.MeshData(vertices, triangles);
	}

	public static void MarchCube(Vector3Int position, float[] cube, List<Vector3> vertices, List<int> triangles, 
		float thresholdDensity = 0.5f, bool smoothTerrain = true, bool flatShaded = true, Dictionary<Vector3, int> vertexIndexDict = null)
	{

		int configIndex = GetCubeConfig(cube, thresholdDensity);


		if (configIndex == 0 || configIndex == 255)
		{
			return;
		}

		int edgeIndex = 0;
		for (int i = 0; i < 5; i++)
		{
			for (int p = 0; p < 3; p++)
			{
				int index = LookupTable.TriangleTable[configIndex, edgeIndex];
				if (index == -1)
				{
					return;
				}

				Vector3 vert1 = position + LookupTable.EdgeTable[index, 0];

				Vector3 vert2 = position + LookupTable.EdgeTable[index, 1];
				Vector3 vertPosition;
				if (smoothTerrain)
				{
					float vert1Sample = cube[LookupTable.EdgeIndices[index, 0]];
					float vert2Sample = cube[LookupTable.EdgeIndices[index, 1]];
					float difference = vert2Sample - vert1Sample;
					if (difference == 0)
					{
						difference = thresholdDensity;
					}
					else
					{
						difference = (thresholdDensity - vert1Sample) / difference;
					}
					vertPosition = vert1 + ((vert2 - vert1) * difference);
				}
				else
				{
					vertPosition = (vert1 + vert2) / 2;
				}

				if (flatShaded)
				{
					vertices.Add(vertPosition);
					triangles.Add(vertices.Count - 1);
				}
				else
				{
					triangles.Add(IndexForVertex(vertPosition, vertexIndexDict, vertices));
				}
				edgeIndex++;
			}
		}

	}

	static int IndexForVertex(Vector3 vertex, Dictionary<Vector3, int> vertexIndexDict, List<Vector3> vertices)
	{
		if (vertexIndexDict.ContainsKey(vertex))
		{

			return vertexIndexDict[vertex];
		}
		//the vertex doesn't exist so we create it
		vertices.Add(vertex);
		int index = vertices.Count - 1;
		vertexIndexDict[vertex] = index;

		return index;
	}
}
