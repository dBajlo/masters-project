﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Noise 
{
	static int myHeight;
	static bool hasBorder = false;
	public static System.Int32 seed = 42;

	public static float[,,] ConvertHeightMapForMarchingCubes(int width, int height, int length,
		Vector3 offset, int maxHeight, int bottomPadding, float[,] heightMap)
	{
		float[,,] noiseMap = new float[width + 1, height + 1, length + 1];

		float pointDensity;
		float halfMaxHeight = maxHeight / 2;

		for (int x = 0; x < width + 1; x++)
		{
			for (int z = 0; z < length + 1; z++)
			{
				float thresholdHeight = Mathf.Clamp(maxHeight * heightMap[x, z], 0.1f, maxHeight - 1f);
				float interpolationRange = thresholdHeight > halfMaxHeight 
					? maxHeight - thresholdHeight 
					: thresholdHeight;
				interpolationRange = Mathf.Clamp(interpolationRange, 0.1f, maxHeight - 1);
				float flatValueRange = maxHeight - 2 * interpolationRange;

				for (int y = 0; y < height + 1; y++)
				{
					float verticalPosition = y + offset.y;
					if(verticalPosition < bottomPadding)
                    {
						pointDensity = Constants.GROUND_DENSITY;
                    }
                    else if(verticalPosition <= thresholdHeight)
                    {
						if(thresholdHeight <= halfMaxHeight)
                        {
							pointDensity = Mathf.Lerp(0, 0.5f, verticalPosition / interpolationRange);
						}
                        else if(verticalPosition < flatValueRange)
                        {
							pointDensity = 0;
                        }
                        else
                        {
							pointDensity = Mathf.Lerp(0, 0.5f, (verticalPosition - flatValueRange) / interpolationRange);
						}
						
                    }
                    else
                    {
						if(thresholdHeight <= halfMaxHeight)
                        {
							if(verticalPosition < 2 * interpolationRange)
                            {
								pointDensity = Mathf.Lerp(0.5f, 1, (verticalPosition - interpolationRange)
							/ interpolationRange);
							}
                            else
                            {
								pointDensity = 1;
                            }
                        }
                        else
                        {
							pointDensity = Mathf.Lerp(0.5f, 1, (verticalPosition - thresholdHeight)
							/ interpolationRange);
						}
						
                    }
                    
					noiseMap[x, y, z] = pointDensity;
				}
			}
		}
		return noiseMap;
	}

	public static float[,] GenerateHeightMap(int width, int length, int octaves, float scale, Vector3 offset)
    {
		float[,] noiseMap = new float[width, length];
		for(int i = 0; i < width; i++)
        {
			for(int j = 0; j < length; j++)
            {
				float noisePlateauThreshold = 0.2f;
				float value = GetPerlinNoise(i + offset.x, j + offset.z, scale, octaves);
				if (value < noisePlateauThreshold) value = 0;
				else value = (value - noisePlateauThreshold) / (1 - noisePlateauThreshold);
				noiseMap[i, j] = value;
            }
        }
		return noiseMap;
    }

	public static float[,,] GenerateNoiseMapPerlin(int width, int height, int length, bool usesFalloff, float thresholdDensity,
		float falloffPower, int octaves, float scale, float persistence, float lacunarity, int perlinNum, Vector3 offset)
	{
		float[,,] noiseMap = new float[width + 1, height + 1, length + 1];
		myHeight = height;

		Vector3 center = new Vector3(width / 2, height / 2, length / 2);
		float radius = width / 2;
		float squareRadius = radius * radius;

		float pointDensity;
		for (int x = 0; x < width + 1; x++)
		{
			for (int y = 0; y < height + 1; y++)
			{
				for (int z = 0; z < length + 1; z++)
				{
					if (hasBorder && IsBorder(x, y, z, 1, width, height, length))
					{
						pointDensity = 1;
					}
					else {
						//float bounds = thresholdDensity * falloffPower;
						//pointDensity *= thresholdDensity + (1 / bounds - bounds) * (float)y / (height);
						
						pointDensity = GetPointDensity(new Vector3(x, y, z), octaves, scale, 
							persistence, lacunarity, offset, perlinNum, usesFalloff);
						if (usesFalloff)
						{
							pointDensity *= falloffPower * ((float)y / (height));
						}
					}
					noiseMap[x, y, z] = pointDensity;
				}
			}
		}
		
		return noiseMap;
    }

	public static float[,,] GenerateNoiseMap(FastNoiseLite noise, int width, int height, int length, float scale, 
		Vector3 offset, float threshold)
    {
		float[,,] noiseMap = new float[width + 1, height + 1, length + 1];

		for(int i = 0; i < width + 1; i++)
        {
			for(int j = 0; j < height + 1; j++)
            {
				for(int k = 0; k < length + 1; k++)
                {
					float value;
					if (hasBorder && IsBorder(i, j, k, 1, width, height, length))
					{
						value = 1;
					}
                    else
                    {
						value = 0.5f - noise.GetNoise((i + offset.x) / scale,
															  (j + offset.y) / scale,
															  (k + offset.z) / scale) / 2f;
						value = value > threshold ? 1 : 0;
					}
					
					
					noiseMap[i, j, k] = value;
                }
            }
        }

		return noiseMap;
    }
	public static bool IsBorder(int x, int y, int z, int borderWidth, int width, int height, int length)
	{
		for (int i = 0; i < borderWidth; i++)
		{
			if (x == 0 + i || y == 0 + i || z == 0 + i || x == width - i || y == height - i || z == length - i)
			{
				return true;
			}
		}

		return false;
	}

	public static float GetPointDensity(Vector3 coords, int octaves, float scale, float persistence, 
		float lacunarity, Vector3 offset, int perlinNum, bool usesFalloff)
	{
		float density = 0;
		float amplitude = 1;
		float frequency = 0.25f;
		float noise = 0;
		for (int i = 0; i < octaves; i++)
		{
			noise = Perlin3D(((float)coords.x + offset.x) / scale * frequency, 
							((float)coords.y + offset.y) / scale * frequency, 
							((float)coords.z + offset.z) / scale * frequency, perlinNum) * amplitude;

			if (i == 0)
			{
				density += noise;
			}
			//reduce the influence of octaves in greater heights to prevent floating elements
			else if (!usesFalloff || coords.y < myHeight / 2)
			{	
				//scale noise to range [-0.5A, 0.5A]
				density += noise - amplitude * 0.5f;
			}
			amplitude *= persistence;
			frequency *= lacunarity;
		}
		return density;
	}


	public static float Perlin3D(float x, float y, float z, int perlinNum)
	{
		float ab = Mathf.PerlinNoise(x, y);
		float bc = Mathf.PerlinNoise(y, z);
		
		float sum = ab + bc;
		if (perlinNum > 2)
		{
			float ac = Mathf.PerlinNoise(x, z);
			sum += ac;
		}
		if (perlinNum > 3)
		{
			float ba = Mathf.PerlinNoise(y, x);
			sum += ba;
		}
		if (perlinNum > 4)
		{
			float cb = Mathf.PerlinNoise(z, y);
			sum += cb;
		}
		if (perlinNum > 5)
		{
			float ca = Mathf.PerlinNoise(z, x);
			sum += ca;
		}

		float result = sum / (float)perlinNum;
		return result;
	}

	public static float GetNormalizedNoise(float x, float y, float scale, int octaves = 1)
	{
		float value = 0;
		float amplitude = 1f;
		float frequency = 1f;
		for (int i = 0; i < octaves; i++)
		{
			value += 2 * (Mathf.PerlinNoise(frequency * x / scale + octaves * scale + seed, 
											frequency * y / scale + octaves * scale + seed) - 0.5f) * amplitude;
			amplitude *= 0.5f;
			frequency *= 2f;
		}

		return Mathf.Clamp(value, -1, 1);
	}

	public static float GetPerlinNoise(float x, float y, float scale, int octaves = 1)
	{
		float value = 0;
		float amplitude = 1f;
		float frequency = 1f;
		float valueChange;
		for (int i = 0; i < octaves; i++)
		{
			valueChange = Mathf.PerlinNoise(frequency * x / scale + octaves * scale + seed,
											frequency * y / scale + octaves * scale + seed) * amplitude;
			if (i == 0)
			{
				value += valueChange;
			}
			else
			{
				value += valueChange - amplitude * 0.5f;
			}
			amplitude *= 0.5f;
			frequency *= 2f;
		}
		return Mathf.Clamp01(value);
	}

	public static float GetThresholdNoise(float x, float y, float scale, int octaves = 1)
	{
		float magicNumber = 40;
		float value = GetNormalizedNoise(x, y, scale, octaves);
		return Mathf.Clamp01(value * value * magicNumber);
	}

	public static float WhiteNoise(float u, float v)
    {
		float magicLargeNumber = 43758.5453f;
		Vector2 uv = new Vector2(u, v);
		Vector2 magicFloat2 = new Vector2(12.9898f, 78.233f);
		float dot = Vector2.Dot(uv, magicFloat2);
		dot = Mathf.Sin(dot) * magicLargeNumber;
		float frac = dot - Mathf.Floor(dot);
		return frac;
    }
}


