using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public static float maxWormSegmentLength = 4f;
    public static int maxWormSegmentNumber = 50;
    public static float maxWormLength = maxWormSegmentLength * maxWormSegmentNumber + maxWormSegmentRadius;

    //4, 10
    public static int minWormSegmentRadius = 5;
    public static int maxWormSegmentRadius = 6;

    public static float AIR_DENSITY = 1;
    public static float GROUND_DENSITY = 0;

    public static float CHUNK_CONTAIN_WORM_HEAD_THRESHOLD = 0.9f;
}
