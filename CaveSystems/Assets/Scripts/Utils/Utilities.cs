using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Utilities
{
    public static Vector3Int getChunkCoordsFromPosition(Vector3 position, int width, int height, int length)
    {
        return new Vector3Int(Mathf.FloorToInt(position.x / width),
                              Mathf.FloorToInt(position.y / height),
                              Mathf.FloorToInt(position.z / length));
    }

    public static Vector3Int getChunkLocationFromChunkCoords(Vector3Int chunkCoords, int width, int height, int length)
    {
        return new Vector3Int(chunkCoords.x * width, chunkCoords.y * height, chunkCoords.z * length);
    }

    public static bool chunkOutOfRenderDistance(Vector3Int chunkCoords, Vector3Int centerChunkCoords, float sqrRenderDistance)
    {
        return Vector3.SqrMagnitude((Vector3)chunkCoords - (Vector3)centerChunkCoords) > sqrRenderDistance;
    }

    public static Vector3Int getLocalCoordsInsideChunk(Vector3Int chunkCoords, Vector3 globalCoords, 
        int width, int height, int length)
    {
        Vector3 localCoords = globalCoords - (Vector3)getChunkLocationFromChunkCoords(chunkCoords, width, height, length);
        return new Vector3Int(Mathf.RoundToInt(localCoords.x), Mathf.RoundToInt(localCoords.y), 
            Mathf.RoundToInt(localCoords.z));
    }

    public static bool IsPointInsideChunk(Vector3 point, Vector3Int chunkLocation, int width, int height, int length)
    {
        return point.x >= chunkLocation.x && point.x < chunkLocation.x + width &&
            point.y >= chunkLocation.y && point.y < chunkLocation.y + height &&
            point.z >= chunkLocation.z && point.z < chunkLocation.z + length;
    }

    //ONLY CHECKS 2 DIMENSIONS, DOES NOT ACCOUNT FOR HEIGHT
    public static bool DoesWormReachChunk(Vector3 wormHeadPosition, Vector3Int chunkLocation, int width, int length)
    {
        float chunkRadius = Mathf.Sqrt(width * width + length * length);
        Vector3 chunkCenter = new Vector3(chunkLocation.x + width / 2.0f, 0, chunkLocation.z + length / 2.0f);
        Vector3 wormHeadY0 = new Vector3(wormHeadPosition.x, 0, wormHeadPosition.z);
        return Vector3.Magnitude(wormHeadY0-chunkCenter) - chunkRadius < Constants.maxWormLength;
    }

    //ONLY RETURNS CHUNKS THE WORM PASSES THROUGH, NOT ALL CHUNKS AFFECTED BY THE CAVE RADIUS
    public static List<Vector3Int> GetChunksWormPassesThrough(List<Vector3> segmentPositions, int width, int height, int length)
    {
        List<Vector3Int> chunks = new List<Vector3Int>();
        foreach(Vector3 position in segmentPositions)
        {
            Vector3Int chunkCoord = getChunkCoordsFromPosition(position, width, height, length);
            if (!chunks.Contains(chunkCoord)) chunks.Add(chunkCoord);
        }
        return chunks;
    }

    public static float TrimExtremeValues(float value, Vector2 range, float trimmingFactor = 2)
    {
        float halfRangeSize = (range.y - range.x) / 2;
        float normalizedValue = (value - halfRangeSize) / (halfRangeSize);
        float newValue = normalizedValue * trimmingFactor;
        newValue = Mathf.Clamp(newValue, -1, 1);
        newValue = value * halfRangeSize + halfRangeSize;
        return newValue;
    }

    public static HashSet<Vector3Int> GetAllChunksInRadius(Vector3 center, float radius, int width, int height, int length)
    {
        HashSet<Vector3Int> activeChunks = new HashSet<Vector3Int>();
        Vector2Int bbx = new Vector2Int(Mathf.FloorToInt((center.x - radius) / width), 
                                        Mathf.FloorToInt((center.x + radius) / width));
        Vector2Int bby = new Vector2Int(Mathf.FloorToInt((center.y - radius) / height),
                                        Mathf.FloorToInt((center.y + radius) / height));
        Vector2Int bbz = new Vector2Int(Mathf.FloorToInt((center.z - radius) / length),
                                        Mathf.FloorToInt((center.z + radius) / length));
        for(int i = bbx.x; i <= bbx.y; i++)
        {
            for(int j = bby.x; j <= bby.y; j++)
            {
                for(int k = bbz.x; k <= bbz.y; k++)
                {
                    activeChunks.Add(new Vector3Int(i, j, k));
                }
            }
        }
        return activeChunks;
    }

    public static void PrintHashSet(HashSet<Vector3> set)
    {
        foreach(Vector3 vector in set)
        {
            Debug.Log(vector);
        }
    }

    public static string VertexToString(Vector3 vertex)
    {
        return vertex.x + ", " + vertex.y + ", " + vertex.z;
    }

    public static void PrintFloatArray(float[,,] array, int width, int height, int length)
    {
        for (int i = 0; i < width + 1; i++)
        {
            for (int j = 0; j < height + 1; j++)
            {
                for (int k = 0; k < length + 1; k++)
                {
                    Debug.Log(array[i, j, k]);
                }
            }
        }
    }

    public static float GetBuildCavePointValueVariableRadius(Vector3Int segmentLocalCoords, int i, int j, int k, int segmentRadius,
        Vector3 segmentLocation, float currentDensity, float wormSegmentRadiusNoiseScale, float lerpDensity = -1)
    {
        if (lerpDensity < 0) lerpDensity = Constants.AIR_DENSITY;
        Vector3 terrainPoint = new Vector3Int(i, j, k);
        float distance = Vector3.Distance(terrainPoint, segmentLocalCoords);
        float MAGIC_NUMBER = 1f;
        if (distance < 0.01f)
        {
            return lerpDensity;
        }
        Vector3 norm = (terrainPoint - segmentLocalCoords) * (1 / distance);
        float radiusModification =
            1 - Mathf.Abs(Noise.GetNormalizedNoise(norm.x + norm.y + segmentLocation.x,
            norm.z + norm.y + segmentLocation.z, wormSegmentRadiusNoiseScale, 2)) / MAGIC_NUMBER;
        float modifiedRadius = Mathf.Max(0.1f, segmentRadius * radiusModification);
        float newValue = Mathf.Lerp(lerpDensity, currentDensity,
            Mathf.Clamp01(distance / modifiedRadius));
        return newValue;
    }

    public static bool NeedsNewVerticalChunk(float[,,] terrainMap, int width, int height, int length)
    {
        if (terrainMap == null) return false;
        for (int x = 0; x < width + 1; x++)
        {
            for (int z = 0; z < length + 1; z++)
            {
                if(terrainMap[x, height, z] <= 0.5f)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static List<PerlinWorm> GetAllWormsPassingThroughChunk(Vector3Int chunkCoords, List<PerlinWorm> loadedWorms)
    {
        List<PerlinWorm> activeWorms = new List<PerlinWorm>();
        foreach (PerlinWorm worm in loadedWorms)
        {
            if (worm.WormPassesThroughChunk(chunkCoords))
            {
                activeWorms.Add(worm);
            }
        }
        return activeWorms;
    }

    public struct MeshData
    {
        public List<Vector3> vertices;
        public List<int> triangles;
        public MeshData(List<Vector3> _vertices, List<int> _triangles)
        {
            vertices = _vertices;
            triangles = _triangles;
        }
    }

    public struct ColumnDensityCallback
    {
        public Vector3Int originChunkCoords;
        public List<float[,,]> densities;
        public Action<List<float[,,]>, Vector3Int> callback;

        public ColumnDensityCallback(Vector3Int originChunkCoords, List<float[,,]> densities, 
            Action<List<float[,,]>, Vector3Int> callback)
        {
            this.originChunkCoords = originChunkCoords;
            this.densities = densities;
            this.callback = callback;
        }
    }

    public struct ColumnMeshDataCallback
    {
        public Vector3Int originChunkCoords;
        public List<MeshData> columnMeshData;
        public Action<List<Utilities.MeshData>, Vector3Int> callback;

        public ColumnMeshDataCallback(Vector3Int originChunkCoords, List<MeshData> columnMeshData, 
            Action<List<MeshData>, Vector3Int> callback)
        {
            this.originChunkCoords = originChunkCoords;
            this.columnMeshData = columnMeshData;
            this.callback = callback;
        }
    }
}
