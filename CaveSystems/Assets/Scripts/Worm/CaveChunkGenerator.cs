using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaveChunkGenerator : MonoBehaviour
{
    public int width = 64, height = 64, length = 64;
	public bool autoUpdate = true;

	List<Vector3> vertices = new List<Vector3>();
	List<int> triangles = new List<int>();
	float[,,] terrainMap;
	public Material chunkMaterial;
	public List<PerlinWorm> worms = new List<PerlinWorm>();
	public WormGenerator wormGenerator;
	public GameObject wormSegmentPrefab;
	public GameObject caveMarkerPrefab;
	public System.Int32 seed = 42;
	public bool drawTerrain = true;
	[Range(1,100)]
	public int segmentNumber = 20;
	[Range(1,4)]
	public float segmentLength = 2;
	public Vector3 wormHeadPosition = Vector3.forward;

	//worms
	public float wormDirectionNoiseScale = 37;
	public int wormDirectionNoiseOctaves = 3;
	public float wormSegmentRadiusNoiseScale = 37;
	public int wormSegmentRadiusNoiseOctaves = 3;
	public bool drawWorm = false;
	GameObject chunk;

	// Start is called before the first frame update
	void Start()
    {
		Constants.minWormSegmentRadius = 4;
		Constants.maxWormSegmentRadius = 4;
		GenerateCave();
	}

	public void GenerateCave()
    {
		Noise.seed = seed;
		worms.Clear();
		wormGenerator = new WormGenerator(wormSegmentPrefab, caveMarkerPrefab, width, height, length, 
			wormDirectionNoiseScale, wormDirectionNoiseOctaves, wormSegmentRadiusNoiseScale, 
			wormSegmentRadiusNoiseOctaves, segmentLength);
		worms = wormGenerator.GenerateDummyWorm(segmentNumber, wormHeadPosition, drawWorm);
		terrainMap = new float[width + 1, height + 1, length + 1];
		for (int i = 0; i < width + 1; i++)
		{
			for (int j = 0; j < height + 1; j++)
			{
				for (int k = 0; k < length + 1; k++)
				{
					terrainMap[i, j, k] = IsBorder(i, j, k, width, height, length) ? 1 : 1;
				}
			}
		}
		if (drawTerrain)
		{
			RedrawTerrain(Vector3Int.zero);
		}
	}

    public void BuildCaves()
    {
		int wormNum = 0;
		Vector3Int chunkCoords = Vector3Int.zero;
		foreach(PerlinWorm worm in worms)
        {
			wormNum++;
			if(Utilities.DoesWormReachChunk(worm.headPosition, Vector3Int.zero, width, length))
            {
				//Debug.Log("worm " + wormNum + "reached chunk");
				for(int l = 0; l < worm.segmentPositions.Count; l++)
                {
					Vector3 segment = worm.segmentPositions[l];
					int segmentRadius = worm.segmentRadiuses[l];

					if (Utilities.IsPointInsideChunk(segment, Vector3Int.zero, width, height, length))
                    {
						Vector3Int localCoords = Utilities.getLocalCoordsInsideChunk(chunkCoords, segment, width, height, length);
						int startX = Mathf.Max(0, localCoords.x - segmentRadius);
						int endX = Mathf.Min(width + 1, localCoords.x + segmentRadius);
						int startY = Mathf.Max(0, localCoords.y - segmentRadius);
						int endY = Mathf.Min(height + 1, localCoords.y + segmentRadius);
						int startZ = Mathf.Max(0, localCoords.z - segmentRadius);
						int endZ = Mathf.Min(length + 1, localCoords.z + segmentRadius);
						for (int i = startX; i < endX; i++)
                        {
							for (int j = startY; j < endY; j++)
							{
								for (int k = startZ; k < endZ; k++)
								{
									terrainMap[i, j, k] =
									Utilities.GetBuildCavePointValueVariableRadius(localCoords, i, j, k,
									segmentRadius, segment, terrainMap[i, j, k], wormSegmentRadiusNoiseScale, 
									Constants.GROUND_DENSITY);
								}
							}
						}
					}
                }
            }
        }
    }

	float GetBuildCavePointValue(Vector3Int segmentLocalCoords, int i, int j, int k, int segmentRadius)
    {
		float distance = Vector3.Distance(new Vector3(i, j, k), segmentLocalCoords);
		float parameter = Mathf.Clamp01(distance / segmentRadius);
		float newValue = Mathf.Lerp(Constants.GROUND_DENSITY, terrainMap[i, j, k],
			parameter);
		return newValue;
	}

    bool IsBorder(int i, int j, int k, int width, int height, int length)
    {
        return i == 0 || i == width || j == 0 || j == height || k == 0 || k == length;
    }

	public void RedrawTerrain(Vector3Int chunkCoords)
	{
		if(chunk != null)
        {
			Destroy(chunk);
		}
		
		ClearMeshData();
		BuildCaves();
		CreateMeshData();
		//print("Br vrhova u meshu" + vertices.Count);

		BuildMesh(chunkCoords);
	}


	void CreateMeshData()
	{
		Dictionary<Vector3, int> vertexIndexDict = new Dictionary<Vector3, int>();
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				for (int z = 0; z < length; z++)
				{
					float[] cube = new float[8];
					for (int i = 0; i < 8; i++)
					{
						Vector3Int corner = new Vector3Int(x, y, z) + LookupTable.CornerTable[i];
						cube[i] = terrainMap[corner.x, corner.y, corner.z];
					}
					MarchingCubes.MarchCube(new Vector3Int(x, y, z), cube, vertices, 
						triangles, 0.5f, true, false, vertexIndexDict);
				}
			}
		}

	}



	void ClearMeshData()
	{
		vertices.Clear();
		triangles.Clear();
	}

	void BuildMesh(Vector3Int chunkCoords)
	{
		Vector3Int chunkLocation = Utilities.getChunkLocationFromChunkCoords(chunkCoords, width, height, length);
		chunk = new GameObject();
		chunk.transform.parent = transform;
		chunk.AddComponent<Chunk>();
		Chunk chunkScript = chunk.GetComponent<Chunk>();
		chunkScript.Initiate(chunkLocation, chunkCoords, chunkMaterial);
		chunkScript.BuildMesh(vertices, triangles);
	}
}
