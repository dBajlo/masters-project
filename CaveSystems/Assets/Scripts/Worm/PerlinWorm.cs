using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinWorm
{
    public Vector3 headPosition;
    public int maxSegmentNumber;
    public GameObject segmentPrefab;
    public float segmentLength;
    public Transform parent;
    public List<Vector3> segmentPositions;
    public List<HashSet<Vector3Int>> segmentChunks;
    public List<int> segmentRadiuses;
    public HashSet<Vector3Int> activeChunks;

    List<GameObject> drawnSegments;

    int width, height, length;
    public float directionNoiseScale;
    public int directionNoiseOctaves;
    public float segmentRadiusNoiseScale;
    public int segmentRadiusNoiseOctaves;

    public float splitDirectionNoiseScale = 1.1f;
    public float segmentThresholdDecrement = 0.06f;

    public PerlinWorm(Vector3 _headPosition, GameObject _segmentPrefab, Transform _parent,
        Vector3Int chunkDimensions,
        float _directionNoiseScale, int _directionNoiseOctaves, float _radiusNoiseScale, int _radiusNoiseOctaves,
        int _maxSegmentNumber, float _segmentLength = 2)
    {
        headPosition = _headPosition;
        maxSegmentNumber = Mathf.Min(Constants.maxWormSegmentNumber, _maxSegmentNumber);
        segmentPrefab = _segmentPrefab;
        segmentLength = Mathf.Min(Constants.maxWormSegmentLength, _segmentLength);
        parent = _parent;
        segmentPositions = new List<Vector3>();
        segmentRadiuses = new List<int>();
        segmentChunks = new List<HashSet<Vector3Int>>();
        drawnSegments = new List<GameObject>();
        activeChunks = new HashSet<Vector3Int>();

        width = chunkDimensions.x;
        height = chunkDimensions.y;
        length = chunkDimensions.z;

        directionNoiseScale = _directionNoiseScale;
        directionNoiseOctaves = _directionNoiseOctaves;
        segmentRadiusNoiseScale = _radiusNoiseScale;
        segmentRadiusNoiseOctaves = _radiusNoiseOctaves;
}

    public void GenerateWorm(Vector3 headPosition, Vector3 startDirection, int segmentNumber)
    {
        Vector3 currentSegmentPosition = headPosition;
        float splitThreshold = 1f;

        for (int i = 0; i < segmentNumber; i++)
        {
            int segmentRadius = GetSegmentRadius(i, headPosition, segmentRadiusNoiseScale, segmentRadiusNoiseOctaves);
            HashSet<Vector3Int> chunksAffectedBySegment = Utilities.GetAllChunksInRadius(
                currentSegmentPosition, segmentRadius, width, height, length);
            segmentChunks.Add(chunksAffectedBySegment);
            activeChunks.UnionWith(chunksAffectedBySegment);
            segmentPositions.Add(currentSegmentPosition);
            segmentRadiuses.Add(segmentRadius);
            
            float angleX = (Noise.GetNormalizedNoise(i, headPosition.x, 
                directionNoiseScale, directionNoiseOctaves) + 1) / 2;
            float angleY = Noise.GetNormalizedNoise(i, headPosition.y, 
                directionNoiseScale, directionNoiseOctaves);
            angleX *= startDirection.x > 0 ? 1 : -1;
            Vector3 direction = Quaternion.AngleAxis(90 * angleX, Vector3.right) *
                                Quaternion.AngleAxis(180 * angleY, Vector3.up) * startDirection;
            if(direction.y > 0)
            {
                direction.y = 0;
            }

            if (ShouldSplit(currentSegmentPosition, splitThreshold))
            {
                splitThreshold = 1f;
                Vector3 splitDirection = GetSplitDirection(direction);
                Vector3 splitHeadPosition = currentSegmentPosition += segmentLength * splitDirection;
                int splitSegmentNumber = GetSplitLength(segmentNumber - i - 1);
                GenerateWorm(splitHeadPosition, splitDirection, splitSegmentNumber);
            }
            else
            {
                splitThreshold -= segmentThresholdDecrement;
            }

            currentSegmentPosition += segmentLength * direction;
        }
    }

    public int GetSegmentRadius(int segmentIndex, Vector3 headPosition, float radiusNoiseScale, int radiusNoiseOctaves)
    {
        return Mathf.RoundToInt(Constants.minWormSegmentRadius +
                Constants.maxWormSegmentRadius * Noise.GetThresholdNoise(segmentIndex, headPosition.z, 
                radiusNoiseScale, radiusNoiseOctaves));
    }

    public bool WormPassesThroughChunk(Vector3Int chunkCoords)
    {
        return activeChunks.Contains(chunkCoords);
    }

    public bool SegmentAffectsChunk(int segmentIndex, Vector3Int chunkCoords)
    {
        return segmentChunks[segmentIndex].Contains(chunkCoords);
    }

    public bool ShouldSplit(Vector3 segmentPosition, float splitThreshold)
    {
        float u = segmentPosition.x + segmentPosition.y;
        float v = segmentPosition.z + segmentPosition.y;
        int octaves = 3;
        float value = Noise.GetPerlinNoise(u, v, directionNoiseScale, octaves);
        return value > splitThreshold;
    }

    public Vector3 GetSplitDirection(Vector3 originalDirection)
    {
        Vector3 splitDirection = Vector3.Cross(Vector3.up, originalDirection);
        // half of the time flip the direction of split
        if(Noise.GetNormalizedNoise(splitDirection.x, splitDirection.z, 
            splitDirectionNoiseScale) > 0) {
            splitDirection *= -1;
        }
        //make the split angle not 90 degrees
        float splitRotation = Noise.GetNormalizedNoise(splitDirection.z, 
            splitDirection.x, splitDirectionNoiseScale);
        splitDirection = Quaternion.AngleAxis(30 * splitRotation, Vector3.up) 
            * splitDirection;
        return splitDirection;
    }

    public int GetSplitLength(int remainingSegmentNumber)
    {
        return remainingSegmentNumber;
    }

    public static Vector3 GetWormStartDirection(Vector3 headPosition, float scale)
    {
        int octaves = 3;
        Vector3 direction = new Vector3(Noise.GetNormalizedNoise(headPosition.y * 53451.1f, headPosition.z * 41645.3f, scale, octaves),
                           Noise.GetNormalizedNoise(headPosition.x * 16767.3f, headPosition.z * 1437.13f, scale, octaves) - 1,
                           Noise.GetNormalizedNoise(headPosition.x * 5366.5f, headPosition.y * 3442.8f, scale, octaves))
            .normalized;
        return direction;
    }

    public void DrawWorm()
    {
        for(int i = 0; i < segmentPositions.Count; i++)
        {
            Vector3 currentSegmentPosition = segmentPositions[i];
            float segmentRadius = segmentRadiuses[i];
            GameObject segment = GameObject.Instantiate(segmentPrefab, parent);
            segment.transform.position = currentSegmentPosition;
            segment.transform.localScale = Vector3.one * segmentRadius;
            drawnSegments.Add(segment);
        }
    }

    public void DrawCaveMarker(GameObject markerPrefab)
    {
        GameObject marker = GameObject.Instantiate(markerPrefab, parent);
        marker.transform.position = segmentPositions[0];
        drawnSegments.Add(marker);
    }

    public void DeleteDrawnSegments()
    {
        int count = drawnSegments.Count;
        for(int i = count-1; i >= 0; i--)
        {
            GameObject segment = drawnSegments[i];
            drawnSegments.RemoveAt(i);
            GameObject.Destroy(segment);
        }
    }
}
