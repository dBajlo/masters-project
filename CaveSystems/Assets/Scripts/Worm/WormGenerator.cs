using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormGenerator
{
    public GameObject segmentPrefab;
    public GameObject caveMarkerPrefab;
    public float wormDirectionNoiseScale;
    public int wormDirectionNoiseOctaves;
    public float wormSegmentRadiusNoiseScale;
    public int wormSegmentRadiusNoiseOctaves;
    public GameObject wormHolder;
    public int wormNumber = 10;
    public int width, height, length;
    public float segmentLength = 2;

    Vector3Int chunkDimensions;
    float wormStartDirectionScale = 15.1f;

    public WormGenerator(GameObject _segmentPrefab, GameObject _caveMarkerPrefab, int width, int height, int length,
        float _wormDirectionNoiseScale, int _wormDirectionNoiseOctaves, float _wormSegmentRadiusNoiseScale,
        int _wormSegmentRadiusNoiseOctaves, float _segmentLength)
    {
        segmentPrefab = _segmentPrefab;
        caveMarkerPrefab = _caveMarkerPrefab;
        wormHolder = GameObject.Find("WormHolder");
        this.width = width;
        this.height = height;
        this.length = length;
        chunkDimensions = new Vector3Int(width, height, length);
        wormDirectionNoiseScale = _wormDirectionNoiseScale;
        wormDirectionNoiseOctaves = _wormDirectionNoiseOctaves;
        wormSegmentRadiusNoiseScale = _wormSegmentRadiusNoiseScale;
        wormSegmentRadiusNoiseOctaves = _wormSegmentRadiusNoiseOctaves;
        segmentLength = _segmentLength;
    }

    public PerlinWorm GenerateWorm(Vector3Int chunkLocation, bool caveMarkerEnabled, int segmentNumber)
    {
        Vector3 headPosition = chunkLocation + new Vector3(width / 2, height - 1, length / 2);
        PerlinWorm worm = WormWithNewHead(headPosition, segmentNumber);
        Vector3 startDirection = PerlinWorm.GetWormStartDirection(headPosition, wormStartDirectionScale);
        worm.GenerateWorm(headPosition, startDirection, segmentNumber);
        if (caveMarkerEnabled)
        {
            worm.DrawCaveMarker(caveMarkerPrefab);
        }
        return worm;
    }



    public List<PerlinWorm> GenerateDummyWorm(int segmentNumber, Vector3 headPosition, bool drawWorm)
    {
        Vector3 headPosition2 = new Vector3(width - 1 , height - 4, length - 1);
        PerlinWorm worm = WormWithNewHead(headPosition, segmentNumber);
        Vector3 startDirection = (Vector3.zero - headPosition).normalized;
        worm.GenerateWorm(headPosition, startDirection, segmentNumber);
        if(drawWorm) worm.DrawWorm();
        var worms = new List<PerlinWorm>();
        worms.Add(worm);
        return worms;
    }

    public PerlinWorm WormWithNewHead(Vector3 headPosition, int segmentNumber)
    {
        return new PerlinWorm(headPosition, segmentPrefab, wormHolder.transform, chunkDimensions, 
            wormDirectionNoiseScale, wormDirectionNoiseOctaves,
            wormSegmentRadiusNoiseScale, wormSegmentRadiusNoiseOctaves,
            segmentNumber, segmentLength);
    }
}
