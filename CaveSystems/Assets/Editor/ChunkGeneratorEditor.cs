using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CaveChunkGenerator))]
public class ChunkGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        CaveChunkGenerator visualizer = (CaveChunkGenerator)target;
        if (DrawDefaultInspector())
        {
            if (visualizer.autoUpdate)
            {
                visualizer.GenerateCave();
            }
        }
        if (GUILayout.Button("Generate"))
        {
            visualizer.GenerateCave();
        }
    }
}
