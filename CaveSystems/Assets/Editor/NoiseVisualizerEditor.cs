using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(NoiseVisualizer))]
public class NoiseVisualizerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        NoiseVisualizer visualizer = (NoiseVisualizer)target;
        if (DrawDefaultInspector())
        {
            if (visualizer.autoUpdate)
            {
                visualizer.GenerateNoise();
            }
        }
        if (GUILayout.Button("Generate"))
        {
            visualizer.GenerateNoise();
        }
    }
}
